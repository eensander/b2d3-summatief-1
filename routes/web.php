<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return redirect('dashboard');
});
Route::get('/dashboard', 'DashboardController@show')->name('dashboard');

/*
Route::get('/set', function () {
    return redirect('dashboard.sets.items.index');
});
*/





// Route::get('/sets', 'Dashboard\SetController@showItems')->name('dashboard.sets.items');
// Route::get('/sets/items', 'Dashboard\ItemController@index')->name('dashboard.sets.items');


/* Sketch:
 *
 * I. Overview of all sets defined
 * 	/sets/
 *
 * II. Overview of one set. View categories in set or all categories (all). (Also dashboard things perhaps)
 * 	/sets/{set_id}
 *
 * IIIa. Information about a chosen category within this set. Info about categorising is available, and all items will be too.
 * 	/sets/{set_id}/{cat_id}/
 *
 * IIIb. All items within a set.
 * 	/sets/{set_id}/all/
 *
 * IV. One item of a certain set. The category is only persent for navigation purposes and furthermore discarded at this point.
 * 	/sets/{set_id}/{cat_id|all}/{item_id}
 *
 * https://laravel.com/docs/5.1/controllers#restful-nested-resources
 *
 */

/*
 +--------+-----------+-----------------------------------------+-------------------------+
 | Domain | Method    | URI                                     | Name                    |
 +--------+-----------+-----------------------------------------+-------------------------+
 |        | GET|HEAD  | /                                       |                         |
 |        | GET|HEAD  | api/user                                |                         |
 |        | GET|HEAD  | dashboard                               | dashboard               |
 |        | GET|HEAD  | sets                                    | sets.index              |
 |        | POST      | sets                                    | sets.store              |
 |        | GET|HEAD  | sets/create                             | sets.create             |
 |        | PUT|PATCH | sets/{set}                              | sets.update             |
 |        | DELETE    | sets/{set}                              | sets.destroy            |
 |        | GET|HEAD  | sets/{set}                              | sets.show               |
 |        | GET|HEAD  | sets/{set}/cats                         | sets.cats.index         |
 |        | POST      | sets/{set}/cats                         | sets.cats.store         |
 |        | GET|HEAD  | sets/{set}/cats/create                  | sets.cats.create        |
 |        | GET|HEAD  | sets/{set}/cats/{cat}                   | sets.cats.show          |
 |        | PUT|PATCH | sets/{set}/cats/{cat}                   | sets.cats.update        |
 |        | DELETE    | sets/{set}/cats/{cat}                   | sets.cats.destroy       |
 |        | GET|HEAD  | sets/{set}/cats/{cat}/edit              | sets.cats.edit          |
 |        | GET|HEAD  | sets/{set}/cats/{cat}/items             | sets.cats.items.index   |
 |        | POST      | sets/{set}/cats/{cat}/items             | sets.cats.items.store   |
 |        | GET|HEAD  | sets/{set}/cats/{cat}/items/create      | sets.cats.items.create  |
 |        | GET|HEAD  | sets/{set}/cats/{cat}/items/{item}      | sets.cats.items.show    |
 |        | PUT|PATCH | sets/{set}/cats/{cat}/items/{item}      | sets.cats.items.update  |
 |        | DELETE    | sets/{set}/cats/{cat}/items/{item}      | sets.cats.items.destroy |
 |        | GET|HEAD  | sets/{set}/cats/{cat}/items/{item}/edit | sets.cats.items.edit    |
 |        | GET|HEAD  | sets/{set}/edit                         | sets.edit               |
 +--------+-----------+-----------------------------------------+-------------------------+
*/


//
// Route::resource('/sets/items', 'ItemController', [
//     'as' => 'sets'
// ]);


// Route::resource('sets', 'SetController');

Route::resource('sets', 'SetController');
Route::resource('sets.cats', 'CategoryController');
Route::resource('sets.cats.items', 'ItemController');
