<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemMetadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_metadata', function (Blueprint $table) {
            $table->bigInteger('item_id');
            $table->string('key');
            $table->string('value')->nullable();
            $table->timestamps();
            $table->unique( array('item_id','key') );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_metadata');
    }
}
