<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('sets')->insert([
            'set_name' => "Pokemon Cards",
            'metadata_keys' => json_encode([
            	'nationalPokedexNumber',
				'img_url',
				'hp',
				'supertype'
			]),
            'created_at' => date("Y-m-d H:i:s")
        ]);

    }
}
