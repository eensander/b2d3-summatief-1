<?php

use Illuminate\Database\Seeder;

class ItemMetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    $pokemon_cards_json = file_get_contents('https://api.pokemontcg.io/v1/cards');
    $pokemon_cards = json_decode($pokemon_cards_json, 1);

    $counter = 1;

    foreach($pokemon_cards['cards'] as $card){

      $card['hp'] = isset($card['hp']) ? $card['hp'] : NULL;

      DB::table('item_metadata')->insert([
        'item_id' => $counter,
        'key' => 'DEFAULT',
        'value' => json_encode([
			'hp' => $card['hp'],
			'img_url' => $card['imageUrl'],
			'nationalPokedexNumber' => isset($card['nationalPokedexNumber']) ? $card['nationalPokedexNumber'] : NULL,
			'is_pokemon' => isset($card['nationalPokedexNumber']) && !is_null($card['nationalPokedexNumber'])
        ])
      ]);

      $counter++;
    }

    }

}
