<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('categories')->insert([
			[
				'set_id'    => 1,
				'cat_name'  => 'Is pokemon',
				'cat_key'   => 'supertype',
				'cat_value' => 'Pokémon'
			], [
				'set_id'    => 1,
				'cat_name'  => 'Is trainer',
				'cat_key'   => 'supertype',
				'cat_value' => 'Trainer'
			], [
				'set_id'    => 1,
				'cat_name'  => 'Is energie',
				'cat_key'   => 'supertype',
				'cat_value' => 'Energy'
			]
		]);
    }

}
