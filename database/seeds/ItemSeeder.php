<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		//DB::table('items')->truncate();

		$amount_items = 50;

		$pokemon_cards_json = file_get_contents('https://api.pokemontcg.io/v1/cards?pageSize=' . $amount_items);
		$pokemon_cards = json_decode($pokemon_cards_json, 1);

		echo "Found " . count($pokemon_cards['cards']) . " cards" . PHP_EOL;

		$i = 0;
		foreach($pokemon_cards['cards'] as $card)
		{

			echo "Card " . ++$i . " : " . $card['name'] . PHP_EOL;

			$card['hp'] = isset($card['hp']) ? $card['hp'] : NULL;
			$card['nationalPokedexNumber'] = isset($card['nationalPokedexNumber']) ? $card['nationalPokedexNumber'] : NULL;

			$item_data = [
				'nationalPokedexNumber' => isset($card['nationalPokedexNumber']) ? $card['nationalPokedexNumber'] : NULL ,
				'img_url'   => $card['imageUrl'],
				'hp'        => $card['hp'],
				'supertype' => $card['supertype']
			];


			$item_id = DB::table('items')->insertGetId([
				'set_id' => 1,
				'item_name' => $card['name'],
				'item_purchase_value' => rand(0, 10) < 2 ? rand(30, 1000) : rand(2, 30),
				'item_current_value' => rand(0, 10) < 1 ? rand(30, 3000) : rand(1, 30),
				// 'item_data' => json_encode([
				// 	'nationalPokedexNumber' => isset($card['nationalPokedexNumber']) ? $card['nationalPokedexNumber'] : NULL ,
				// 	'img_url' => $card['imageUrl'],
				// 	'hp'      => $card['hp']
				// ]),
				'created_at' => date("Y-m-d H:i:s")
			]);

			foreach($item_data as $item_data_k => $item_data_v)
			{
				DB::table('item_metadata')->insert([
					'item_id' => $item_id,
					'key' => $item_data_k,
					'value' => $item_data_v,
					'created_at' => date("Y-m-d H:i:s")
				]);
			}


		}

    }
}
