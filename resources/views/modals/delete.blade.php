<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalTestLabel"
	 aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="" id="deleteForm" method="post">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modalTestLabel">Verwijderen</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					@csrf
					@method('DELETE')
					<p>Weet u zeker dat u dit item wilt verwijderen?</p>
				</div>
				<div class="modal-footer">
					<button type="submit" name="" id="confirm-delete" class="btn btn-danger" onclick="submitDelete()">Ja</button>
					<button type="button" class="btn btn-success" data-dismiss="modal">Nee</button>
				</div>
			</div>
		</form>
	</div>
</div>
