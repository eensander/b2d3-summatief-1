@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
@endsection()

@section('content')

    {{--
        @forelse ($items as $item)
            <li>{{ $item->item_name }}</li>
        @empty
            <p>No items defined.</p>
        @endforelse
    --}}

    <div class="row">
        <div class="col-md-12">

            @unless(empty($items))

                @include('modals/delete')

                <div class="card">

                    <div class="card-header card-header-primary">
                        <h4 class="card-title">{{ $content['card-title'] }}</h4>
                        <p class="card-category">{{ $content['card-subtitle'] }}</p>
                    </div>

                    <div class="card-body">

						@include('components.buttons')

                        <div class="table-responsive">

                            <table class="table" id="items-table">

                                <thead>

									<tr>

									@foreach($items_header as $header_item)
										<th>{{ $header_item }}</th>
									@endforeach

	                                </tr>

                                </thead>

                                <tbody>

                                @foreach ($items as $item)

                                    <tr>
										@foreach ($item as $col)
                                        <td>
											{!! $col !!}
                                        </td>
										@endforeach
                                    </tr>

                                @endforeach

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            @else
                <p>Geen items in deze verzameling</p>
            @endif

        </div>
    </div>

@endsection


@section('scripts')
	<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
	<script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
@endsection

@section('script_ready')
	$('#items-table').DataTable();
@endsection
