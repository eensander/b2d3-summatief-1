@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="{{ route('sets.cats.show', [$set, $cat]) }}">{{ $cat->cat_name ?? 'Alle categoriën' }}</a> >
	<a class="navbar-brand" href="#">{{ $item->item_name }}</a>
@endsection()

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @elseif ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Verzameling {{ $set['set_name'] }} - Item {{ $item['item_name'] }}</h4>
                    <p class="card-category">Lorem ipsum dollar biem bam skurt pr pr bla la la ding dang big bang</p>
                </div>
                <div class="card-body">

                    <form action="{{ route('sets.cats.items.update', [ $set, $cat ?? 'all', $item ] ) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="datatype" value="data">

                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Item ID</label>
                                    <input type="text" class="form-control" disabled value="{{ $item->item_id }}">
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Item naam</label>
                                    <input type="text" class="form-control" name="item_name"
                                           value="{{ $item['item_name'] }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Aanschafwaarde</label>
                                    <input type="text" class="form-control" name="item_purchase_value"
                                           value="{{ $item['item_purchase_value'] }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Dagwaarde</label>
                                    <input type="text" class="form-control" name="item_current_value"
                                           value="{{ $item['item_current_value'] }}">
                                </div>
                            </div>

                        </div>


                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                        <div class="clearfix"></div>
                    </form>

                </div>

            </div>


        </div>
    </div>


    <div class="row">
        <div class="col-md-12">


            <div class="card">

                <div class="card-header card-header-primary">
                    <h4 class="card-title">Metadata</h4>
                    <p class="card-category">Metadata van dit item</p>
                </div>

                <div class="card-body">

                    <form action="{{ route( 'sets.cats.items.update', [$set, $cat ?? 'all', $item] ) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="datatype" value="metadata">

                        <div class="table-responsive">
                            <table class="table table-bordered" id="data-table">
                                {{-- <caption>Metadata van item</caption> --}}
                                <thead class="text-primary thead-light">
                                <tr>
                                    <th>Key</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($item->getMetadata() as $data_k => $data_v)
                                    <tr>
                                        <td>{{ $data_k }}</td>
                                        <td><input type="text" name="metadata[{{ $data_k }}]" class="form-control"
                                                   value="{{ $data_v ?? 'None' }}" style="{{ $data_v ? '' : 'background-color: #feefef' }}"></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection


@section('script_ready')



@endsection
