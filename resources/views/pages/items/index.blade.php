@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="">{{ $cat->cat_name ?? 'Alle categoriën' }}</a> >
	<a class="navbar-brand" href="">Items</a>
@endsection()

@section('content')

	{{--
		@forelse ($items as $item)
			<li>{{ $item->item_name }}</li>
		@empty
			<p>No items defined.</p>
		@endforelse
	--}}

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif

	<div class="row">
		<div class="col-md-12">

			@unless(empty($items))

				@include('modals/delete')

				<div class="card">

					<div class="card-header card-header-primary">
						<h4 class="card-title">{{ $content['card-title'] }}</h4>
						<p class="card-category">{{ $content['card-subtitle'] }}</p>
					</div>

					<div class="card-body">

						@include('components.buttons')

						<div class="table-responsive">

							<table class="table" id="items-table">

								<thead>

								<tr>

									@foreach($items_header as $header_item)
										<th>{{ $header_item }}</th>
									@endforeach

								</tr>

								</thead>

								<tbody>

								@foreach ($items as $item)

									<tr>

										@foreach ($item as $index => $col)
											<td>
												{!! $col !!}
											</td>
										@endforeach

									</tr>

								@endforeach

								</tbody>

							</table>

						</div>

					</div>

				</div>

			@else
				<p>Geen items in deze verzameling</p>
			@endif

		</div>
	</div>

@endsection


@section('scripts')
	<!-- Plugin for Light Box 2 -->
	<script src="/assets/js/plugins/lightbox.js"></script>
	<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
	<script src="/assets/js/plugins/jquery.dataTables.min.js"></script>

	<script type="text/javascript">
		function deleteData(s, c, i) {

			var set = s;
			var cat = (c == 0 ? 'all' : c);
			var item = i;

			var url = '{{ route("sets.cats.items.destroy", ["set"     => ":set",
															 "cat"     => ":cat",
															  "item" => ":item"]) }}';

			url = url.replace(':set', set);
			url = url.replace(':cat', cat);
			url = url.replace(':item', item);

			$("#deleteForm").attr('action', url);
		}

		function submitDelete() {
			$("#deleteForm").submit();
		}
	</script>
@endsection

@section('script_ready')
	$('#items-table').DataTable();
@endsection
