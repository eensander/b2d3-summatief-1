@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="{{ route('sets.cats.show', [$set, $cat ?? 'all']) }}">{{ $cat->cat_name ?? 'Alle categoriën' }}</a> >
	<a class="navbar-brand" href="#">Nieuw item</a>
@endsection()

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title">Verzameling {{ $set->set_name }} - Nieuw Item</h4>
					<p class="card-category">Dit is een sample text, ofzoiets</p>
				</div>
				<div class="card-body">

					<form action="{{ route('sets.cats.items.store', [ $set, $cat ?? 'all', $item ] ) }}" method="POST">
						@csrf
						@method('POST')

						<div class="row">

							<div class="col-md-12">
								<div class="form-group">
									<label class="bmd-label-floating">Item naam</label>
									<input type="text" class="form-control" name="item_name"
										   value="{{ $item['item_name'] }}">
								</div>
							</div>
						</div>

						<div class="row">

							<div class="col-md-6">
								<div class="form-group">
									<label class="bmd-label-floating">Aanschafwaarde</label>
									<input type="text" class="form-control" name="item_purchase_value"
										   value="{{ $item['item_purchase_value'] }}">
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="bmd-label-floating">Dagwaarde</label>
									<input type="text" class="form-control" name="item_current_value"
										   value="{{ $item['item_current_value'] }}">
								</div>
							</div>

						</div>

						<div class="table-responsive">
							<table class="table table-bordered" id="data-table">
								<thead class="text-primary thead-light">
								<tr>
									<th>Key</th>
									<th>Value</th>
								</tr>
								</thead>
								<tbody>
								@foreach($set->getMetadataKeys() as $data_k)
									<tr>
										<td>{{ $data_k }}</td>

										@if($data_k == 'supertype')
											<td>
												<select class="form-control" name="metadata[{{ $data_k }}]">
													@foreach($set->categories as $value)
														<option value="{{ $value->cat_value }}">{{ $value->cat_value }}</option>
													@endforeach
												</select>
											</td>
										@else
											<td><input type="text" name="metadata[{{ $data_k }}]" class="form-control"
													   value=""></td>
										@endif
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>

						<button type="submit" class="btn btn-success pull-right">Maak aan</button>
					</form>

				</div>
			</div>
		</div>
	</div>

@endsection
