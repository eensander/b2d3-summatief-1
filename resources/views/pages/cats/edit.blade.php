@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="">{{ $cat->cat_name ?? 'Alle categoriën' }}</a> >
	<a class="navbar-brand" href="">Bewerken</a>
@endsection()

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @elseif ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Basisinformatie</h4>
                    <p class="card-category">Pas hier de algemene informatie aan van deze categorie</p>
                </div>
                <div class="card-body">

                    <form action="{{ route('sets.cats.update', [ $set, $cat ] ) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Category ID</label>
                                    <input type="text" class="form-control" disabled value="{{ $cat->cat_id }}">
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Categorie naam</label>
                                    <input type="text" class="form-control" name="cat_name"
                                           value="{{ $cat->cat_name }}">
                                </div>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Category ID</label>
                                    <select class="form-control" name="set_id">
										@foreach(App\Set::get() as $i_set)
										<option value="{{ $i_set->set_id }}">{{ $i_set->set_name }}</option>
										@endforeach
									</select>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Categorie Sleutel</label>
                                    <select style="height: 100px;" size="8" class="form-control input-sm" name="cat_key" id="cat_key">

									@foreach(array_keys($cat_keyvalues) as $cat_key)

										<option {{ $cat_key == $cat->cat_key ? 'selected' : '' }} value="{{ $cat_key }}">{{ $cat_key }}</option>

									@endforeach

									</select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Categorie Waarde</label>
                                    <select style="height: 100px;" size="8" class="form-control" name="cat_value" id="cat_value">

									@foreach(array_keys($cat_keyvalues) as $cat_key)

										@foreach($cat_keyvalues[$cat_key] as $cat_value)

											<option
												{{ $cat_key == $cat->cat_key && $cat_value == $cat->cat_value ? 'selected' : '' }}
												style="{{ $cat_key == $cat->cat_key ? '' : 'display: none' }}"
												data-key="{{ $cat_key }}"
												value="{{ $cat_value }}">{{ $cat_value }}</option>

										@endforeach

									@endforeach

									</select>
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                        <div class="clearfix"></div>
                    </form>

                </div>

            </div>


        </div>
    </div>


@endsection


@section('script_ready')

$('#cat_key').on('change', function()
{
	key = $(this).val();
	$('#cat_value option').hide();
	$('#cat_value option[data-key="'+key+'"]').show();
});

@endsection
