@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="">{{ 'Nieuwe categorie' }}</a>
@endsection()

@section('content')

	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header card-header-primary">
					<h4 class="card-title">Nieuwe categorie</h4>
					<p class="card-category">Dit is een sample text, ofzoiets</p>
				</div>
				<div class="card-body">



				</div>
			</div>
		</div>
	</div>

@endsection
