@extends('layout/frame')

@section('page_breadcrumb')
	<a class="navbar-brand" href="{{ route('sets.index') }}">Verzamelingen</a> >
	<a class="navbar-brand" href="{{ route('sets.show', [$set]) }}">{{ $set->set_name }}</a> >
	<a class="navbar-brand" href="">{{ $cat->cat_name ?? 'Alle categoriën' }}</a>
@endsection()

@section('content')

    {{--
        @forelse ($item ?? ''s as $item ?? '')
            <li>{{ $item ?? ''->item_name }}</li>
        @empty
            <p>No items defined.</p>
        @endforelse
    --}}

    <div class="row">
        <div class="col-md-12">

            <div class="card">

                <div class="card-header card-header-primary">
                    <h4 class="card-title">Categoriën</h4>
                    <p class="card-category">Hier vindt u mooie categoriën en kunt u ook categoriën toevoegen I guess</p>
                </div>

                <div class="card-body">

					@include('components.buttons')

                    <div class="table-responsive">

                        <table class="table" id="items-table">

                            <thead>

								<tr>

									<th>id</th>
									<th>Naam</th>
									<th>Filter key en waarde</th>
									<th>Aantal t.o.v populatie</th>
									<th>Percentage t.o.v populatie (%)</th>
									<th class="no-sort">Actions</th>

                                </tr>

								<tr style="background-color: #c33c54; color: white;">
									<td>#</td>
									<td><a class="light-link" href="{{ route('sets.cats.show', ['set' => $set, 'cat' => 'all']) }}">Alle categoriën (populatie)</a></td>
									<td>*</td>
									<td>{{ $set->items()->count() }}</td>
									<td>100 %</td>
									<td>
										<a class="btn btn-info" href="{{ route('sets.cats.items.index', ['set' => $set, 'cat' => 'all']) }}">
											items van cat
										</a>
									</td>
								</tr>

                            </thead>

                            <tbody>


                            @foreach ($categories as $category)

								<tr>
									<td>{{ $category['id'] }}</td>
									<td><a href="{{ route('sets.cats.items.index', ['set' => $set,'cat' => $category['id']]) }}">{{ $category['name'] }}</a></td>
									<td><pre><strong>{{ $category['key'] }}</strong> = {{ $category['value'] }}</pre></td>
									<td>{{ $category['items'] }}</td>
									<td>{{ $category['percentage'] }} %</td>
									<td>
										<a class="btn btn-info" href="{{ route('sets.cats.items.index', ['set' => $set, 'cat' => $category['id']]) }}">
											items van cat
										</a>
										<a class="btn btn-primary" href="{{ route('sets.cats.edit', ['set' => $set, 'cat' => $category['id']]) }}">
											bewerken(?)
										</a>
									</td>
								</tr>

                            @endforeach

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>
    </div>

@endsection

@section('scripts')
	<!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
	<script src="/assets/js/plugins/jquery.dataTables.min.js"></script>
@endsection

@section('script_ready')
	$('#items-table').DataTable({
		columnDefs: [
			{
				 targets: 'no-sort',
				 orderable: false
			}
		]
	});
@endsection
