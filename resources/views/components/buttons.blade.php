@foreach($buttons as $btn)
	<a href="{{ route($btn['url'], $btn['url-params']) }}" class="{{ $btn['class'] }}">{{ $btn['name'] }}</a>
@endforeach
