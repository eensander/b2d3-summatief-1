<div class="sidebar" data-color="purple" data-background-color="white">

  <div class="logo">
    <a class="simple-text logo-normal">
      Kaart Verzameling
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item {{ Route::is('dashboard') ? 'active' : '' }} ">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <i class="material-icons">dashboard</i>
          <p>Dashboard</p>
        </a>
      </li>
      <li class="nav-item {{ Route::is('sets.index') ? 'active' : '' }} ">
        <a class="nav-link" href="{{ route('sets.index') }}">
          <i class="material-icons">collections</i>
          <p>Verzamelingen</p>
        </a>
      </li>
	  <!-- custom nested sidebar -->
	  <ul class="nav">
@foreach(App\Set::get() as $i_set)
		<li class="nav-item {{ isset($set) && Route::is('sets.*') ? 'active' : '' }} ">
		  <a class="nav-link" href="{{ route('sets.cats.index', $i_set) }}">
			<i class="material-icons">dashboard</i>
			<p>{{ $i_set->set_name }}</p>
		  </a>
		</li>
@endforeach
	  </ul>

    </ul>
  </div>
</div>
