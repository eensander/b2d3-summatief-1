<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ItemMetadata extends Model
{

	protected $fillable = [
		'key',
		'item_id',
		'value'
	];

    protected function setKeysForSaveQuery(Builder $query)
    {
      $query
          ->where('key', '=', $this->getAttribute('key'))
          ->where('item_id', '=', $this->getAttribute('item_id'));
      return $query;
    }

	public function item()
	{
		return $this->belongsTo(Item::class, 'item_id', 'item_id');
	}


	public static function getSetKeys($set_id)
	{
		return ItemMetadata::with(['item.set' => function($q) use ($set_id) {
			$q->where('set_id', $set_id);
		}])
			->distinct()
			->pluck('key');
	}
	public static function getSetKeyValues($set_id, $cat_key)
	{
		return ItemMetadata::with([
			'item.set' => function($q) use ($set_id) {
				$q->where('set_id', $set_id);
			}
		])
			->where('key', $cat_key)
			->distinct()
			->pluck('value');
	}

}
