<?php

namespace App;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	//

	protected $primaryKey = 'cat_id';

	public function set()
	{
		return $this->belongsTo('App\Set', 'set_id', 'set_id');
	}


	public function metadataKey()
	{
		return $this->hasMany('App\ItemMetadata', 'key', 'cat_key');
	}

	public function metadataValue()
	{
		return $this->hasMany('App\ItemMetadata', 'value', 'cat_value');
	}

	public function metadataRelation()
	{
		return $this->metadataKey->merge($this->metadataValue);
	}


	public function items()
	{
		/* works- but returns wrong object: Category
		return $this
			->join('item_metadata', function($join) {

				$join
					->on('item_metadata.key', '=', 'categories.cat_key')
					->on('item_metadata.value', '=', 'categories.cat_value');

			})->join('items', 'items.item_id', '=', 'item_metadata.item_id')
			->where('cat_id', $this->cat_id)
			->get();
		*/


		/*
		$item_ids = $this->metadataValue()->get()->pluck('item_id');

		return (Item::find($item_ids));
		*/


		$item_ids = $this
			->join('item_metadata', function($join) {

				$join
					->on('item_metadata.key', '=', 'categories.cat_key')
					->on('item_metadata.value', '=', 'categories.cat_value');

			})->join('items', 'items.item_id', '=', 'item_metadata.item_id')
			->where('cat_id', $this->cat_id)
			->get()->pluck('item_id');

		// dd($item_ids);

		return (Item::find($item_ids));


		/*

		*/

		/*
				$cat_key   = x;
				$cat_value = y;

				SELECT * FROM `items` `I`
				JOIN `ItemMetadata` `IM`
					ON `I`.`item_id` = `IM`.`item_id`

				WHERE `IM`.`key` = :cat_key AND `IM`.`value` = :cat_value
		*/


	}

}
