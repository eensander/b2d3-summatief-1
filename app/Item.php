<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    public $timestamps = true;

    protected $primaryKey = 'item_id';

    protected $fillable = [
        'item_name',
        'item_current_value',
        'item_purchase_value'
    ];

	public function set()
	{
		return $this->belongsTo(Set::class, 'set_id', 'set_id');
	}

	public function metadata()
	{
		return $this->hasMany(ItemMetadata::class, 'item_id', 'item_id');
	}

    public function getMetadata($key = null)
    {
		if (!is_null($key))
		{
			return $this->metadata()->where('key', $key)->pluck('value')->first();
		}
		else
		{
			$metadatas = $this->metadata()->select('key', 'value')->get();

			$metadata_assoc = [];

			foreach($metadatas as $metadata)
			{
				$metadata_assoc[$metadata->key] = $metadata->value;
			}

			return $metadata_assoc;
		}

    }

    public function removeMetadata($key = null)
    {
		if (!is_null($key))
		{
			$metadata = $this->metadata()->where('key', $key)->delete();
		}
		else
		{
			$metadata = $this->metadata()->delete();
		}

    }

	public function setMetadata($a1 = null, $a2 = null)
	{
		$update_array = [];

		if (is_array($a1) && is_null($a2)) // array set(['key1' => 'value1', 'key2' => 'value2']) passed
		{
		 	$update_array = $a1;
		}
		elseif (!is_null($a1) && is_string($a1)) // args set('key', 'value' | null)
		{
			$update_array = [$a1 => $a2];
		}

		$metadata = $this->metadata();

		foreach($update_array as $meta_k => $meta_v)
		{
			if ($metadata->where('key', $meta_k)->exists())
			{
				$meta_key = $metadata->where('key', $meta_k)->first();
			}
			else
			{
				$meta_key = new ItemMetadata();
				$meta_key->item_id = 1;
				$meta_key->key     = $meta_k;
			}

			$meta_key->value = $meta_v;
			$meta_key->save();

		}

	}

}
