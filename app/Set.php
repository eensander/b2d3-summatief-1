<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $primaryKey = 'set_id';

	public function categories()
	{
		return $this->hasMany('App\Category', 'set_id', 'set_id');
	}

	public function items()
	{
		return $this->hasMany('App\Item', 'set_id', 'set_id');
	}

	public function getMetadataKeys()
	{
		return json_decode($this->metadata_keys, true);
	}

}
