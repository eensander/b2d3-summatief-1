<?php

namespace App\Http\Controllers;

use App\Set;
use App\Category;
use Illuminate\Http\Request;

class SetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$content = [
			'card-title' => 'Verzamelingen',
			'card-subtitle' => 'Overzicht van verzamelingen'
		];

		$sets = Set::get();

		$items_header = [
			'Naam',
			'Actions'
		];

		$items_data = [];

		foreach($sets as $set)
		{

			$item_data = [];


			$item_data[0] = '<a href="' . route('sets.cats.index', ['set' => $set['set_id']]) .'">'.$set['set_name'].'</a>';

			$item_data[1] = '<a class="btn btn-primary" href="'. route('sets.cats.index', ['set' => $set['set_id']]) .'">
				bewerken
			</a>
			<a class="btn btn-danger" data-toggle="modal" data-target="#modalTest" href="#">
				verwijderen
			</a>';


			$items_data[] = $item_data;

		}

		$buttons = [
			[
				'name' => 'Nieuwe Verzameling',
				'url' => 'sets.create',
				'url-params' => "",
				'class' => 'btn btn-success'
			]
		];

        return view('pages/sets.index', [
			'items' => $items_data,
			'items_header' => $items_header,
			'content' => $content,
			'buttons' => $buttons
		]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('pages/sets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Set $set)
    {
		return redirect()->route('sets.cats.index', [$set]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
