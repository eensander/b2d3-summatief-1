<?php

namespace App\Http\Controllers;

use App\Set;
use App\Category;
use App\ItemMetadata;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Set $set)
    {

		$set_items = $set->items();

		$category_objects = $set->categories;

		$categories = [];

		foreach($category_objects as $cat_obj)
		{

			$percentage = ($cat_obj->items()->count() / $set_items->count()) * 100;

			// https://stackoverflow.com/a/4483715
			$percentage_disp = $percentage == 0 ?
								'0' :	// percentage IS zero
									($percentage > 0 && $percentage < 0.1) ?
									'< 0.1' : // percentage is LESS than 0.1, but NOT zero (almost zero)
									sprintf('%0.1f', $percentage); // normal behaviour

			$categories[] = [
				'id'         => $cat_obj['cat_id'],
				'name'       => $cat_obj['cat_name'],
				'key'        => $cat_obj['cat_key'],
				'value'      => $cat_obj['cat_value'],
				'items'      => $cat_obj->items()->count() /* . ' / ' . $set_items->count() */,
				'percentage' => $percentage_disp
			];

		}

		$buttons = [
			[
				'name' => 'Nieuwe Categorie',
				'url' => 'sets.cats.create',
				'url-params' => [
					'set' => $set
				],
				'class' => 'btn btn-success'
			]
		];

        return view('pages/cats.index', [
			'set' => $set,
			'categories' => $categories,

			'buttons' => $buttons
		]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Set $set)
    {
		return view('pages/cats.create', [
			'set' => $set
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($set_id, $cat_id)
    {
		return redirect()->route('sets.cats.items.index', [$set_id, $cat_id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Set $set, $cat_id)
    {
		if ($cat_id == 'all')
		{
			return redirect()->route('sets.cats.items.index', [ $set, $cat_id ]);
		}
		else
		{
			$cat = Category::findOrFail($cat_id);

			// $cat_keys = ItemMetadata::with('item')->where('set_id', $set->set_id)->groupBy('key')->get();
			$cat_keys = ItemMetadata::getSetKeys($set->set_id);

			$cat_keyvalues = [];

			foreach($cat_keys as $cat_key)
			{
				$cat_keyvalues[$cat_key] = ItemMetadata::getSetKeyValues($set->set_id, $cat_key);
			}

			return view('pages/cats/edit', [
				'set' => $set,
				'cat' => $cat,
				'cat_keyvalues' => $cat_keyvalues,
			]);
		}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
		$request->validate([
			'cat_name'            => 'required',
			'set_id'              => 'required|exists:sets',
		]);

		$category->update([
			'cat_name' =>  $request->cat_name,
			'set_id'   =>  $request->set_id
		]);

		return redirect()->back()->with('success','Item has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
