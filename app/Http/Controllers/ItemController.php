<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Set;
use App\Category;
use App\Item;
use App\ItemMetadata;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Set $set, $cat_id)
    {

		// return 'items van set met cat ' . $set_id . " - " . $cat_id;

		$cat = $cat_id == 'all' ? null : Category::findOrFail($cat_id);

		if(is_null($cat))
		{
			$items = Item::all();
		}
		else
		{
			$items = $cat->items();
		}

		// dd($items);

		$content = [
			'card-title' => ($cat ? 'Items' : 'Alle items') . ' van verzameling '.$set->set_name . ($cat ? ' met categorie ' . $cat->cat_name : ''),
			'card-subtitle' => 'Verzaleming van kaarten'
		];

		$buttons = [
			[
				'name' => 'Nieuwe Kaart',
				'url' => 'sets.cats.items.create',
				'url-params' => [
					'set' => $set,
					'cat' => $cat ?? 'all'
				],
				'class' => 'btn btn-success'
			]
		];

		if (!is_null($cat))
		{
			$buttons[] = [
				'name' => 'Categorie bewerken',
				'url' => 'sets.cats.edit',
				'url-params' => [
					'set' => $set,
					'cat' => $cat ?? 'all'
				],
				'class' => 'btn btn-primary'
			];
		}

		$items_header = [
			'Image',
			'Naam',
			'Aanschafwaarde',
			'Dagwaarde',
			'Actions'
		];

		$items_data = [];

		foreach($items as $item)
		{
			$itemMetadata = $item->getMetadata();

			$item_data = [];

			$item_data[0] = '<a href="' . htmlspecialchars($itemMetadata['img_url']) .'" data-lightbox="item-image" data-title="'. htmlspecialchars($item['item_name']) .'">
				<img src="' . htmlspecialchars($itemMetadata['img_url']) .'" style="height: 64px; width: auto;" alt="Item Image">
			</a>';

			$item_data[1] = htmlspecialchars($item['item_name']);

			$item_data[2] = htmlspecialchars($item['item_purchase_value']);


			$current_price_color = 'black';
			if ($item['item_current_value'] < $item['item_purchase_value'])
				$current_price_color = 'firebrick';
			elseif ($item['item_current_value'] > $item['item_purchase_value'])
				$current_price_color = 'limegreen';

			$item_data[3] = '<span style="color: '.$current_price_color.'">' . htmlspecialchars($item['item_current_value']) . '</span>';


			$item_data[4] = '<a class="btn btn-primary" href="'. route('sets.cats.items.edit', ['set' => $set, 'cat' => $cat_id, 'item' => $item] ) .'">
				bewerken
			</a>
			<a class="open-DeleteModal btn btn-danger"
			   data-toggle="modal"
			   data-target="#deleteModal"
			   onclick="deleteData('.$item->set_id.','.( $cat->cat_id ?? 0 ).','.$item->item_id.')"
			   href="#">
				verwijderen
			</a>';

			$items_data[] = $item_data;

		}

        return view('pages/items.index', [
			'set' => $set,
			'cat' => $cat_id,

			'items' => $items_data,
			'items_header' => $items_header,
			'content' => $content,

			'buttons' => $buttons

		]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Set $set, $cat_id)
    {
		$cat = $cat_id == 'all' ? null : Category::findOrFail($cat_id);

        return view('pages/items.create', [
        	'set' => $set,
			'cat' => $cat,
			'item' => new Item(),
			'itemMetadata' => new ItemMetadata()
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Set $set, $cat_id)
    {

		$cat = $cat_id == 'all' ? null : Category::findOrFail($cat_id);

		$request->validate([
			'item_name'           =>  'required',
			'item_purchase_value' =>  'required|numeric',
			'item_current_value'  =>  'required|numeric'
		]);

		$item = new Item;
		foreach ($item->getFillable() as $val)
		{
			$item[$val] = $request[$val];
		}

		$item->set_id = $set->set_id;
		$item->save();

		foreach ($request->metadata as $key => $val)
		{
			$metadata = new ItemMetadata;

			$metadata->item_id 	= $item->item_id;
			$metadata->key 		= $key;
			$metadata->value	= $val;

			$metadata->save();
		}

		return redirect()->route('sets.cats.items.index', [
			'set' => $set,
			'cat' => $cat
		])->with('success','Item has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Set $set, $cat, Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Set $set, $cat_id, Item $item)
    {

		$cat = $cat_id == 'all' ? null : Category::findOrFail($cat_id);

        return view('pages/items.edit', [
		  'set' => $set,
		  'cat' => $cat,
		  'item' => $item
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Set $set, $cat, Item $item)
    {
		if ($request->datatype == 'data')
		{

			$request->validate([
				'item_name'           =>  'required',
				'item_purchase_value' =>  'required|numeric',
				'item_current_value'  =>  'required|numeric'
			]);

			$item->update([
				'item_name'           =>  $request->item_name,
				'item_purchase_value' =>  $request->item_purchase_value,
				'item_current_value'  =>  $request->item_current_value
			]);

		}
		elseif ($request->datatype == 'metadata')
		{
			foreach($request->metadata as $metadata_k => $metadata_v)
			{
				$item->setMetadata($metadata_k, $metadata_v == 'None' ? null : $metadata_v );
			}
		}

		return redirect()->back()->with('success','Item has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($set_id, $cat_id, $item_id)
    {
        $item = Item::find($item_id);
        $item->delete();

        return redirect()->back()->with('success','Item has been deleted');
    }
}
