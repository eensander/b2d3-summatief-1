
# B2D3 - Summatief 1 - Verzameling Manager

https://bitbucket.org/eensander/b2d3-summatief-1/

## Online Demo

Op moment (nog) niet beschikbaar

## Installation

### PHP

Dit project is afhankelijk en vereist minimaal PHP7 (getest op PHP 7.2.24) en een DBMS (sqlite is ondersteund). Ook moet de Composer extentie van PHP geïnstalleerd zjin.

Er zijn verschillende methoden om PHP (op windows) te installeren.

Voor de installatieinstructies van PHP is een online methode gebruikt, omdat deze getest is.
De volgende instructies is gekozen omdat deze uitlegt hoe:
 - PHP moet worden geïnstalleerd,
 - de PDO_SQLITE extentie van php moet worden geactiveerd
 - Composer wordt geinstalleerd
 - PHP aan het windows-'PATH' toevoegen (zodat het via CLI overal toegankelijk wordt)

https://www.jeffgeerling.com/blog/2018/installing-php-7-and-composer-on-windows-10

### Project

Als PHP en andere vereisten zijn geïnstalleerd kan het project worden opgezet.
Allereerst wordt deze projectmap naar een algemen locatie uitgepakt.
Vervolgens wordt er een commandprompt geopend.

In dit commandprompt kiezen we eerst om alle nodige composer-componenten te installeren. Dit kan een aantal minuten duren:

```
composer install
```

Dan moet het .env bestand gereed worden gemaakt. Dit bevat specifieke inforamatie over jouw eigen werkomgeving. Dit doe je door `.env.example` naar `.env` te hernoemen. Als je van plan bent `sqlite` te gaan gebruiken, hernoem dan de `.env.sqlite.example` naar `.env`. Dus:

**SQLite op Windows**
```
ren .env.sqlite.example .env
```

**MySQL op Windows**
```
ren .env.example .env
```

De database-instellingen staan ook in het .env bestand. Liggend aan het database-mechanisme dat wordt gebruikt moet dit in het .env bestand worden aangepast. Voor een SQLITE database...

Dan moet er nog een key worden gegenereerd voor deze omgeving, met `php artisan key:generate`
En voila! De werkomgeving is gereed.

### Database initialisatie

Als de werkomgeving gereed is moet de database of het database(-sqlite-)bestand worden geinitialiseerd. Omdat er vanuit het 'code-first' principe is gewerkt wordt de database vanuit de code opgebouwd en geseed.

Het volgende commando voert alle migraties (structuur) bestanden uit en 'seed' daarna gelijk alle data:

```
php artisan migrate:fresh --seed
```

> Als er een rechten-fout ontstaat, probeer dan het bestand aan te maken (op linux: touch database/database.sql) en probeer het bovenstaande commando dan opnieuw

### Running

De webserver is nu klaar om te starten met een commando:

```
php artisan serve
```

Volg de link in de webbrowser en het project moet werkbaar zijn.






## Framework

Deze verzameling-manager is gemaakt in het framework Laravel (dat gebaseerd is op het Symfony-framework).

## Design

Verdere ontwerp aspecten zijn te vinden in de ./design map

### Graphical
Voor de crud-taken is een Open Source dashboard template gebruikt: https://github.com/creativetimofficial/material-dashboard

Een simpel kleurenschema dat gebruikt is is te vinden:
https://coolors.co/333745-eee5e9-f44336-db7f67-dbbea1
#473198



## Naming Conventions

Voor dit project zijn een aantal keuzen gemaakt voor naamgeving.
De gemaakte keuze stampt voort uit de documentatie van laravel.com:

https://laravel.com/docs/5.1/eloquent#eloquent-model-conventions
_"(...) plural name of the class will be used as the table name (...) So, in this case, Eloquent will assume the Flight model stores records in the flights table_

    model   singular    camelCase
    table   plural      underscore_case



# TODO

Hieronder staat een TODO lijst met resterende taken voor de ontwikkelaars.
Deze zijn in het Engels geschreven om het systeem beheerbaar en overdraagbaar te houden in de toekomst.  

 - [/] desribe instructions for installing (correct version of) PHP and sqlite if necessary?

 - [X] Add key's (as json) to a set for default metadata on item creation
 - [X] rename show's to edit.blade.php pages and make it functional
 - [ ] add total profit as column on categories (using sprintf to display, see Percentage-col for reference)
 - [ ] prevent 'All categories' item in sets.cats.index from sorting when sorting on column

 - [X] positive profit on item green (+), negative red (-)

 - [ ] add delete modals to cats, sets
 - [ ] add create page to cats, sets

 - [X] Edit button for items in 'All categories' fixed

 - [ ] in automatic table generation: instead of adding raw value, add type and necessary data with indexes, like: // (item)controller: $item = ['img', ['img_url' => x]] // view: if $item[0] then $item[1]['item_id']

 - [X] category at creation item
